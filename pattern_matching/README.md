Given a pattern and a string input - find if the string follows the same pattern and return true or false.

Examples:

Pattern : "abab", input: "redblueredblue" should return true.
Pattern: "aaaa", input: "asdasdasdasd" should return true.
Pattern: "aabb", input: "xyzabcxzyabc" should return false.

Please code your solution using ruby.

When you're ready to submit, create a new branch for this exercise and open an MR on gitlab so our team to review your work and comment as we would if you were our teammate.
