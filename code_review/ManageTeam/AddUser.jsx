import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Axios from 'axios';
import Autosuggest from 'react-autosuggest';
import { Form, FormControl, FormGroup, ControlLabel, HelpBlock, Button } from 'react-bootstrap';

// set token when request is made
// check them to cancel existing request if new one is made or component is unmounted
let CancelToken = Axios.CancelToken;
let cancel;

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion.
// Calculate the input value for every given suggestion.
const getSuggestionValue = suggestion => (
  suggestion.email
);

// adds each suggestion to list of suggestions in dropdown
const renderSuggestion = suggestion => (
  <span className="dropdown-item">
    {suggestion.email}
  </span>
);

class AddUser extends Component {
	constructor(props) {
		super(props);

		this.state = {
			accessLevel: '',
			value: '',
			shareMessage: `this ${this.props.type}`,
			isValid: false,
			suggestions: [],
			errorState: null,
			errorMessage: '',
		}
	}

	componentWillUnmount = () => {
	  // check for cancel token to see if a request is running
	  // cleanup and cancel existing requests
	  if (cancel != undefined) {
	    cancel();
	  }
	}

	handleInputChange = (e, {newValue}) => {
		this.setState({
			value: newValue,
			errorState: null,
			isValid: this.getValidationState("email", newValue),
		});
	}

	handleAccessChange = (e) => {
		const target = e.target;

		this.setState({
      accessLevel: target.value,
      isValid: this.getValidationState(target.name, target.value),
    });
	}

	// Autosuggest calls this every time suggestions need to update
	onSuggestionsFetchRequested = ({ value }) => {
	  // don't get suggestions until at least 3 characters
	  if (value.length > 2) {

	  	Axios.get(`/access/autocomplete.json?object_id=${this.props.matterSlug}&object_type=Matter&search=${value}`)
	  	  .then((response) => {
	  	    let newData = response.data;

	  	    this.setState({
	  	      suggestions: newData.users,
	  	      shareMessage: newData.access_message,
	  	    });
	  	  }).catch(function(err) {
	  	    console.log(err);
	  	  });
	  }
	}

	getValidationState = (inputType, value) => {
		// Check if an access level is selected and an email with valid format is entered
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if (inputType == "email") {
			let email = value;
			if (email.length < 6 || !this.state.accessLevel) {
				// invalid email if doesn't even have min number of chars
				return false;
			} else {
			  return (re.test(email) && this.state.accessLevel.length > 0);
			}
		} else {
			let accessLevel = value;
			if (accessLevel && this.state.value && re.test(this.state.value)) {
				return true;
			} else {
				return false;
			}
		}
	}

	handleSubmit = (e) => {
		if (e) {e.preventDefault()};
		let token = "null";
		if (document != undefined && document.querySelector('meta[name=csrf-token]') != null) {
			token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
		}

		Axios({
		  method: 'post',
		  url: '/access/grant_access.json',
		  data: {
		  	email: this.state.value,
		  	permission: this.state.accessLevel,
		  	object_id: this.props.objectId,
		  	object_type: this.props.type
		  },
		  headers: {
		    'X-CSRF-Token': token,
        'X-Requested-With': 'XMLHttpRequest',
		  },
		  cancelToken: new CancelToken(function executor(c) {
		    cancel = c;
		  })
		}).then((response) => {
			this.setState({
				accessLevel: '',
				value: '',
				isValid: false,
			});

			// callback to update users in team modal
			this.props.updateCallback();
      cancel = undefined;
		}).catch((err) => {
			console.log(err);
			this.setState({
				errorState: 'error',
				errorMessage: err.response.data.errors,
			});

      cancel = undefined;
		});
	}

	clearForm = () => {
		this.setState({
			accessLevel: '',
			value: '',
			isValid: false,
		});
	}

	// Autosuggest will call this function every time we need to clear suggestions
	onSuggestionsClearRequested = () => {
	  this.setState({
	    suggestions: [],
	  });
	}

	render() {
		const { value, suggestions } = this.state;

		// Autosuggest passes these props to the input.
		const inputProps = {
			type: "email",
		  placeholder: "Enter user's email address",
		  value,
		  onChange: this.handleInputChange,
		  id: "userSearch",
		  name: "email"
		};

		// add custom styling to autosuggest component
		const inputStyle  = {
		  container:                'react-autosuggest__container autosuggest',
		  containerOpen:            'react-autosuggest__container--open',
		  input:                    'react-autosuggest__input matter_search typeahead form-control input-lg',
		  inputOpen:                'react-autosuggest__input--open',
		  inputFocused:             'react-autosuggest__input--focused',
		  suggestionsContainer:     'react-autosuggest__suggestions-container dropdown',
		  suggestionsContainerOpen: 'react-autosuggest__suggestions-container--open dropdown open',
		  suggestionsList:          'react-autosuggest__suggestions-list dropdown-menu typeahead',
		  suggestion:               'react-autosuggest__suggestion',
		  suggestionFirst:          'react-autosuggest__suggestion--first',
		  suggestionHighlighted:    'react-autosuggest__suggestion--highlighted',
		  sectionContainer:         'react-autosuggest__section-container',
		  sectionContainerFirst:    'react-autosuggest__section-container--first',
		  sectionTitle:             'react-autosuggest__section-title'
		};

		return(
			<div className="add-member">
				<h4>Add a member</h4>
				<Form>
					<div className="row">
						<div className="col-sm-7 col-md-5">
							<FormGroup
			          controlId="enterEmailAddress"
			          validationState={this.state.errorState}
			        >
			          <ControlLabel>Email address</ControlLabel>
			          {this.state.errorState ?
			          	<HelpBlock className="pull-right">{this.state.errorMessage}</HelpBlock>
			          : null }
			          <div className="autosuggest-wrapper">
			            <Autosuggest
			              suggestions={suggestions}
			              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
			              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
			              getSuggestionValue={getSuggestionValue}
			              renderSuggestion={renderSuggestion}
			              inputProps={inputProps}
			              theme={inputStyle}
			            />
			          </div>
			        </FormGroup>
		        </div>

		        <div className="col-sm-5 col-md-4">
			        <FormGroup controlId="selectAccessLevel">
		            <FormControl
		            	componentClass="select"
		            	value={this.state.accessLevel}
		            	name="accessLevel"
		            	onChange={this.handleAccessChange}
		            >
			            <option
			            	value=""
			            	disabled
			            >
			            	Select access level...
			            </option>

			            {this.props.currentUserPermission === "edit" ?
	            			<option
	            				className="add_user access_level"
	            				value="edit"
	            			>
	            				Can edit
	            			</option>
	            		: null }

            			<option
            				className="add_user access_level"
            				value="view"
            			>
            				Can view
            			</option>
		            </FormControl>
		          </FormGroup>
		         </div>

		         <div	className="col-md-3 hidden-xs hidden-sm">
			        <Button
			        	type="submit"
			        	className="pull-right btn-auto add_user"
								onClick={this.handleSubmit}
								id="grant_access"
			        	disabled={this.state.isValid ? false : true}
			        >
			          Grant access
			        </Button>
		        </div>
	        </div>
				</Form>

				<div className="row">
					<div className="col-md-9">
						{this.state.isValid ?
							<p className="text-muted">This person will be able to {this.state.accessLevel} {this.state.shareMessage}</p>
						: null }
					</div>

					<div className="col-md-3 hidden-xs hidden-sm">
		        <Button
			        type="reset"
			        onClick={this.clearForm}
		        	bsStyle="link"
		        	className="pull-right"
		        	disabled={this.state.value || this.state.accessLevel.length > 0 ? false : true}
		        >
		        	Cancel
	        	</Button>
					</div>
				</div>

				<div className="row hidden-md hidden-lg">
					<div className="col-xs-12">
						<div className="pull-right">
			        <Button
				        type="reset"
				        onClick={this.clearForm}
			        	bsStyle="link"
			        	disabled={this.state.value || this.state.accessLevel.length > 0 ? false : true}
			        >
			        	Cancel
		        	</Button>
			        <Button
			        	type="submit"
			        	className="btn-auto"
								onClick={this.handleSubmit}
								id="grant_access"
			        	disabled={this.state.isValid ? false : true}
			        >
			          Grant access
			        </Button>
		        </div>
	        </div>
				</div>
			</div>
		);
	}

}

AddUser.propTypes = {
	updateCallback: PropTypes.func.isRequired,
	objectId: PropTypes.number.isRequired,
	type: PropTypes.string,
	objectName: PropTypes.string,
	currentUserPermission: PropTypes.string,
	matterSlug: PropTypes.string,
};

AddUser.defaultProps = {
	type: 'task',
	objectName: '',
	currentUserPermission: 'view',
	matterSlug: '',
};

export default AddUser;
